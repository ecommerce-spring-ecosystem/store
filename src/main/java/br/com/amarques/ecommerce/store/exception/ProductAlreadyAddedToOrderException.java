package br.com.amarques.ecommerce.store.exception;

public class ProductAlreadyAddedToOrderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ProductAlreadyAddedToOrderException(String message) {
        super(message);
    }
}
