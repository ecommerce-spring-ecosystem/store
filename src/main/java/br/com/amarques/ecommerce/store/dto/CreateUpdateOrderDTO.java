package br.com.amarques.ecommerce.store.dto;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class CreateUpdateOrderDTO {

    public final Long customerId;
    public final BigDecimal discount;

}
