package br.com.amarques.ecommerce.store.dto;

import java.math.BigDecimal;
import java.util.List;

import br.com.amarques.ecommerce.store.enums.OrderStatusEnum;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class OrderDTO {

    public final Long id;
    public final Long customerId;
    public final BigDecimal amount;
    public final OrderStatusEnum status;
    public final BigDecimal discount;

    public final String createdAt;
    public final String updatedAt;

    public final List<OrderItemDTO> items;

    public OrderDTO(Long id, Long customerId, BigDecimal amount, OrderStatusEnum status, BigDecimal discount,
            String createdAt, String updatedAt) {
        this.id = id;
        this.customerId = customerId;
        this.amount = amount;
        this.status = status;
        this.discount = discount;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.items = null;
    }
}
