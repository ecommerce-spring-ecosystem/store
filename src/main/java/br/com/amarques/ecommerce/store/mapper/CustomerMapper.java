package br.com.amarques.ecommerce.store.mapper;

import br.com.amarques.ecommerce.store.domain.Customer;
import br.com.amarques.ecommerce.store.dto.CreateUpdateCustomerDTO;
import br.com.amarques.ecommerce.store.dto.CustomerDTO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomerMapper {

    public static Customer toEntity(CreateUpdateCustomerDTO dto) {
        Customer customer = new Customer();
        customer.setName(dto.email);
        customer.setEmail(dto.email);
        return customer;
    }

    public static CustomerDTO toDTO(Customer customer) {
        return new CustomerDTO(customer.getId(), customer.getName(), customer.getEmail());
    }
}
