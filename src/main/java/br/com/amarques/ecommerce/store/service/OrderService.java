package br.com.amarques.ecommerce.store.service;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.amarques.ecommerce.store.domain.Customer;
import br.com.amarques.ecommerce.store.domain.Order;
import br.com.amarques.ecommerce.store.dto.CreateUpdateOrderDTO;
import br.com.amarques.ecommerce.store.dto.OrderDTO;
import br.com.amarques.ecommerce.store.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.store.enums.OrderStatusEnum;
import br.com.amarques.ecommerce.store.exception.ClosedOrderException;
import br.com.amarques.ecommerce.store.exception.NotFoundException;
import br.com.amarques.ecommerce.store.mapper.OrderMapper;
import br.com.amarques.ecommerce.store.repository.OrderRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository repository;
    private final CustomerService customerService;

    @Transactional
    public SimpleEntityDTO create(CreateUpdateOrderDTO dto) {
        Customer customer = customerService.findById(dto.customerId);

        Order order = OrderMapper.toEntity(dto, customer);

        repository.save(order);

        return new SimpleEntityDTO(order.getId());
    }

    @Transactional
    public void update(Long orderId, CreateUpdateOrderDTO orderDTO) {
        Order order = findById(orderId);
        Customer customer = customerService.findById(orderDTO.customerId);

        order.setCustomer(customer);
        order.setDiscount(orderDTO.discount);

        repository.save(order);
    }

    protected Order findById(Long id) {
        Optional<Order> order = repository.findById(id);

        if (!order.isPresent()) {
            throw new NotFoundException(MessageFormat.format("Order [id: {0}] not found.", id));
        }

        return order.get();
    }

    public OrderDTO getById(Long orderId) {
        Order order = findById(orderId);
        return OrderMapper.toDTO(order);
    }

    public List<OrderDTO> getAll() {
        List<Order> orders = repository.findAll();
        if (CollectionUtils.isEmpty(orders)) {
            return Collections.emptyList();
        }
        return orders.stream().map(OrderMapper::toShallowDTO).collect(Collectors.toList());
    }

    @Transactional
    public void delete(Long orderId) {
        Order order = findById(orderId);
        if (OrderStatusEnum.CLOSED.equals(order.getStatus())) {
            throw new ClosedOrderException("You cannot exclude a closed order.");
        }
        repository.delete(order);
    }

    @Transactional
    public void closeOrder(Long orderId) {
        Order order = findById(orderId);
        order.closeOrder();
        repository.save(order);
    }

}
