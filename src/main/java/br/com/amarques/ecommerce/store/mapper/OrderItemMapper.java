package br.com.amarques.ecommerce.store.mapper;

import br.com.amarques.ecommerce.store.domain.OrderItem;
import br.com.amarques.ecommerce.store.dto.OrderItemDTO;
import br.com.amarques.ecommerce.store.formatter.DateFormatter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderItemMapper {

    public static OrderItemDTO toDTO(OrderItem item) {
        return new OrderItemDTO(
                item.getId(),
                item.getProduct().getId(),
                item.getUnitaryValue(),
                item.getQuantity(),
                item.getAmount(),
                DateFormatter.format(item.getCreatedAt()),
                DateFormatter.format(item.getUpdatedAt()));
    }
}
