package br.com.amarques.ecommerce.store.exception;

public class ProductNotFoundInOrderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ProductNotFoundInOrderException(String message) {
        super(message);
    }
}
