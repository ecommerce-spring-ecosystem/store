package br.com.amarques.ecommerce.store.service;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.amarques.ecommerce.store.domain.Customer;
import br.com.amarques.ecommerce.store.dto.CreateUpdateCustomerDTO;
import br.com.amarques.ecommerce.store.dto.CustomerDTO;
import br.com.amarques.ecommerce.store.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.store.exception.EmailAlreadyRegisteredException;
import br.com.amarques.ecommerce.store.exception.NotFoundException;
import br.com.amarques.ecommerce.store.mapper.CustomerMapper;
import br.com.amarques.ecommerce.store.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;

    @Transactional
    public SimpleEntityDTO create(CreateUpdateCustomerDTO dto) {
        Optional<Customer> registeredCustomer = getOneByEmail(dto.email);
        if (registeredCustomer.isPresent()) {
            throw new EmailAlreadyRegisteredException(MessageFormat.format("[{0}] email is already registered",
                    dto.email));
        }

        Customer customer = CustomerMapper.toEntity(dto);
        repository.save(customer);

        return new SimpleEntityDTO(customer.getId());
    }

    public List<CustomerDTO> getAll() {
        List<Customer> customers = repository.findAll();
        if (CollectionUtils.isEmpty(customers)) {
            return Collections.emptyList();
        }

        return customers.stream().map(CustomerMapper::toDTO).collect(Collectors.toList());
    }

    public CustomerDTO getOneById(Long id) {
        Customer customer = findById(id);
        return CustomerMapper.toDTO(customer);
    }

    @Transactional
    public void update(Long id, CreateUpdateCustomerDTO dto) {
        Customer customer = findById(id);
        customer.setName(dto.name);
        repository.save(customer);
    }

    @Transactional
    public void delete(Long id) {
        Customer customer = findById(id);
        repository.delete(customer);
    }

    private Optional<Customer> getOneByEmail(String email) {
        return repository.findByEmail(email);
    }

    protected Customer findById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException(MessageFormat.format("Customer [id: {0}] not found.", id)));
    }
}
