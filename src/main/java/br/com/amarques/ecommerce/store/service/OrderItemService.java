package br.com.amarques.ecommerce.store.service;

import java.text.MessageFormat;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.amarques.ecommerce.store.client.CatalogClient;
import br.com.amarques.ecommerce.store.domain.Order;
import br.com.amarques.ecommerce.store.domain.OrderItem;
import br.com.amarques.ecommerce.store.domain.Product;
import br.com.amarques.ecommerce.store.dto.CreateUpdateOrderItemDTO;
import br.com.amarques.ecommerce.store.dto.OrderItemDTO;
import br.com.amarques.ecommerce.store.dto.ProductDTO;
import br.com.amarques.ecommerce.store.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.store.exception.NotFoundException;
import br.com.amarques.ecommerce.store.mapper.OrderItemMapper;
import br.com.amarques.ecommerce.store.repository.OrderRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderItemService {

    private final OrderRepository orderRepository;
    private final OrderService orderService;
    private final CatalogClient catalogClient;

    @Transactional
    public SimpleEntityDTO addItem(Long orderId, CreateUpdateOrderItemDTO itemDTO) {
        Order order = orderService.findById(orderId);
        ProductDTO productDTO = catalogClient.getProductById(itemDTO.productId);
        Product product = new Product(productDTO.id);

        OrderItem orderItem = order.addItem(product, itemDTO.quantity, productDTO.value);

        orderRepository.save(order);

        return new SimpleEntityDTO(orderItem.getId());
    }

    @Transactional
    public void editItem(Long orderId, Long itemId, CreateUpdateOrderItemDTO itemDTO) {
        Order order = orderService.findById(orderId);
        ProductDTO productDTO = catalogClient.getProductById(itemDTO.productId);

        order.editItem(itemId, new Product(productDTO.id), itemDTO.quantity, productDTO.value);

        orderRepository.save(order);
    }

    @Transactional
    public void deleteItem(Long orderId, Long itemId) {
        Order order = orderService.findById(orderId);

        order.removeItem(itemId);

        orderRepository.save(order);
    }

    public OrderItemDTO getItemByOrderIdAndItemId(Long orderId, Long itemId) {
        Order order = orderService.findById(orderId);
        Optional<OrderItem> orderItem = order.getItemById(itemId);

        if (!orderItem.isPresent()) {
            throw new NotFoundException(MessageFormat.format("Order Item [id: {0}] not found.", itemId));
        }

        return OrderItemMapper.toDTO(orderItem.get());
    }
}
