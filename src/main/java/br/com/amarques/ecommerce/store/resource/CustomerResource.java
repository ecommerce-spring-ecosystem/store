package br.com.amarques.ecommerce.store.resource;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.amarques.ecommerce.store.dto.CreateUpdateCustomerDTO;
import br.com.amarques.ecommerce.store.dto.CustomerDTO;
import br.com.amarques.ecommerce.store.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.store.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/customers")
public class CustomerResource {

    private final CustomerService service;

    @PostMapping
    public ResponseEntity<SimpleEntityDTO> create(@RequestBody CreateUpdateCustomerDTO dto) {
        log.debug("REST request to create a new Customer: {}", dto);

        SimpleEntityDTO simpleEntityDTO = service.create(dto);

        return new ResponseEntity<>(simpleEntityDTO, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<CustomerDTO>> getAll() {
        log.debug("REST request to get all Customers");

        List<CustomerDTO> customers = service.getAll();

        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDTO> getById(@PathVariable Long id) {
        log.debug("REST request to get a Customer [id: {0}]", id);

        CustomerDTO customer = service.getOneById(id);

        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> edit(@PathVariable Long id, @RequestBody CreateUpdateCustomerDTO dto) {
        log.debug("REST request to update a Customer [id: {0}] [dto: {1}]", id, dto);

        service.update(id, dto);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete a Customer [id: {0}]", id);

        service.delete(id);

        return ResponseEntity.ok().build();
    }
}
