package br.com.amarques.ecommerce.store.exception;

public class ClosedOrderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ClosedOrderException(String message) {
        super(message);
    }
}
