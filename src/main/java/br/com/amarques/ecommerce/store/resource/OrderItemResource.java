package br.com.amarques.ecommerce.store.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.amarques.ecommerce.store.dto.CreateUpdateOrderItemDTO;
import br.com.amarques.ecommerce.store.dto.OrderItemDTO;
import br.com.amarques.ecommerce.store.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.store.service.OrderItemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/orders/{orderId}/items")
public class OrderItemResource {

    private final OrderItemService service;

    @PostMapping
    public ResponseEntity<SimpleEntityDTO> addItem(@PathVariable Long orderId,
            @RequestBody CreateUpdateOrderItemDTO itemDTO) {
        log.debug("REST request to add a new Item [{0}] to Order: [id: {1}]", itemDTO, orderId);

        SimpleEntityDTO simpleEntityDTO = service.addItem(orderId, itemDTO);

        return new ResponseEntity<>(simpleEntityDTO, HttpStatus.CREATED);
    }

    @PutMapping("/{itemId}")
    public ResponseEntity<Void> editItem(@PathVariable Long orderId, @PathVariable Long itemId,
            @RequestBody CreateUpdateOrderItemDTO itemDTO) {
        log.debug("REST request to edit a Item [id: {0} - DTO: {1}] Order: [id: {2}]", itemId, itemDTO, orderId);

        service.editItem(orderId, itemId, itemDTO);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{itemId}")
    public ResponseEntity<Void> removeItem(@PathVariable Long orderId, @PathVariable Long itemId) {
        log.debug("REST request to delete an Item [id: {0}]", itemId);

        service.deleteItem(orderId, itemId);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{itemId}")
    public ResponseEntity<OrderItemDTO> getById(@PathVariable Long orderId, @PathVariable Long itemId) {
        log.debug("REST request to get an Item [orderId: {0} - itemId {1}]", orderId, itemId);

        OrderItemDTO itemDTO = service.getItemByOrderIdAndItemId(orderId, itemId);

        return new ResponseEntity<>(itemDTO, HttpStatus.OK);
    }
}
