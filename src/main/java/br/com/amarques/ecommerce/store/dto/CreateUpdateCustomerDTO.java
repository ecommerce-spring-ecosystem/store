package br.com.amarques.ecommerce.store.dto;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class CreateUpdateCustomerDTO {

    public final String name;
    public final String email;

}
