package br.com.amarques.ecommerce.store.exception;

public class OrderItemNotFoundInOrderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public OrderItemNotFoundInOrderException(String message) {
        super(message);
    }

}
