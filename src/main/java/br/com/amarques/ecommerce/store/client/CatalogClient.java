package br.com.amarques.ecommerce.store.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.amarques.ecommerce.store.dto.ProductDTO;

@FeignClient("catalog")
public interface CatalogClient {

    @RequestMapping("/products/{productId}")
    ProductDTO getProductById(@PathVariable Long productId);
}
