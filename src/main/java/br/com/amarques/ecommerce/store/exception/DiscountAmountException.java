package br.com.amarques.ecommerce.store.exception;

public class DiscountAmountException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DiscountAmountException(String message) {
        super(message);
    }
}
