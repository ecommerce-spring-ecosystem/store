package br.com.amarques.ecommerce.store.dto;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class ProductDTO {

    public final Long id;
    public final String name;
    public final BigDecimal value;

}
