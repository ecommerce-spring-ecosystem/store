package br.com.amarques.ecommerce.store.resource;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.amarques.ecommerce.store.dto.CreateUpdateOrderDTO;
import br.com.amarques.ecommerce.store.dto.OrderDTO;
import br.com.amarques.ecommerce.store.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.store.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/orders")
public class OrderResource {

    private final OrderService service;

    @PostMapping
    public ResponseEntity<SimpleEntityDTO> create(@RequestBody CreateUpdateOrderDTO orderDTO) {
        log.debug("REST request to create a new Order: {}", orderDTO);

        SimpleEntityDTO simpleEntityDTO = service.create(orderDTO);

        return new ResponseEntity<>(simpleEntityDTO, HttpStatus.CREATED);
    }

    @PutMapping("/{orderId}")
    public ResponseEntity<Void> update(@PathVariable Long orderId, @RequestBody CreateUpdateOrderDTO orderDTO) {
        log.debug("REST request to update an Order [id: {0}] [dto: {1}]", orderId, orderDTO);

        service.update(orderId, orderDTO);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/{orderId}/close")
    public ResponseEntity<Void> close(@PathVariable Long orderId) {
        log.debug("REST request to close an Order [id: {0}]", orderId);

        service.closeOrder(orderId);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<OrderDTO> getById(@PathVariable Long orderId) {
        log.debug("REST request to get an Order [id: {0}]", orderId);

        OrderDTO orderDTO = service.getById(orderId);

        return new ResponseEntity<>(orderDTO, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<OrderDTO>> getAll() {
        log.debug("REST request to get all Orders");

        List<OrderDTO> orders = service.getAll();

        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<Void> deleteById(@PathVariable Long orderId) {
        log.debug("REST request to delete an Order [id: {0}]", orderId);

        service.delete(orderId);

        return ResponseEntity.ok().build();
    }

}
