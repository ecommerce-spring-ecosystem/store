package br.com.amarques.ecommerce.store.enums;

public enum OrderStatusEnum {

    OPEN,
    CLOSED;
}
