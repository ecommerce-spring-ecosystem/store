package br.com.amarques.ecommerce.store.mapper;

import java.util.stream.Collectors;

import br.com.amarques.ecommerce.store.domain.Customer;
import br.com.amarques.ecommerce.store.domain.Order;
import br.com.amarques.ecommerce.store.dto.CreateUpdateOrderDTO;
import br.com.amarques.ecommerce.store.dto.OrderDTO;
import br.com.amarques.ecommerce.store.formatter.DateFormatter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderMapper {

    public static OrderDTO toShallowDTO(Order order) {
        return new OrderDTO(
                order.getId(),
                order.getCustomer().getId(),
                order.getAmount(),
                order.getStatus(),
                order.getDiscount(),
                DateFormatter.format(order.getCreatedAt()),
                DateFormatter.format(order.getUpdatedAt()));
    }

    public static OrderDTO toDTO(Order order) {
        return new OrderDTO(
                order.getId(),
                order.getCustomer().getId(),
                order.getAmount(),
                order.getStatus(),
                order.getDiscount(),
                DateFormatter.format(order.getCreatedAt()),
                DateFormatter.format(order.getUpdatedAt()),
                order.getItems().stream().map(OrderItemMapper::toDTO).collect(Collectors.toList()));
    }

    public static Order toEntity(CreateUpdateOrderDTO dto, Customer customer) {
        Order order = new Order();
        order.setCustomer(customer);
        order.setDiscount(dto.discount);
        return order;
    }
}
