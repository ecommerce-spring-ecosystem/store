package br.com.amarques.ecommerce.store.dto;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class OrderItemDTO {

    public final Long id;
    public final Long productId;
    public final BigDecimal unitaryValue;
    public final Integer quantity;
    public final BigDecimal amount;

    public final String createdAt;
    public final String updatedAt;

}
