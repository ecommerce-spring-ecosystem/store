package br.com.amarques.ecommerce.store.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.amarques.ecommerce.store.domain.Order;
import br.com.amarques.ecommerce.store.enums.OrderStatusEnum;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findAllByCustomerId(Long customerId);

    List<Order> findAllByStatus(OrderStatusEnum open);

    List<Order> findAllByStatusAndCustomerId(OrderStatusEnum open, Long customerId);

}
