package br.com.amarques.ecommerce.store.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter(value = AccessLevel.PROTECTED)
@Getter
@EqualsAndHashCode
@Entity
@Table(name = "order_itens")
@SequenceGenerator(name = "order_item_pk", sequenceName = "order_item_id_seq", allocationSize = 1, initialValue = 1)
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_item_pk")
    @Column(name = "id")
    private Long id;

    @Embedded
    private Product product;

    @Column(name = "unitary_value")
    private BigDecimal unitaryValue;

    @Column(name = "quantity")
    private Integer quantity;

    @CreationTimestamp
    @Setter(value = AccessLevel.PRIVATE)
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Setter(value = AccessLevel.PRIVATE)
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    public BigDecimal getAmount() {
        return unitaryValue.multiply(BigDecimal.valueOf(quantity));
    }

}
