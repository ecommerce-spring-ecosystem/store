package br.com.amarques.ecommerce.store.domain;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.util.CollectionUtils;

import br.com.amarques.ecommerce.store.enums.OrderStatusEnum;
import br.com.amarques.ecommerce.store.exception.ClosedOrderException;
import br.com.amarques.ecommerce.store.exception.DiscountAmountException;
import br.com.amarques.ecommerce.store.exception.OrderItemNotFoundInOrderException;
import br.com.amarques.ecommerce.store.exception.ProductAlreadyAddedToOrderException;
import br.com.amarques.ecommerce.store.exception.ProductNotFoundInOrderException;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
@Table(name = "orders")
@SequenceGenerator(name = "order_pk", sequenceName = "order_id_seq", allocationSize = 1, initialValue = 1)
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_pk")
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @Setter(value = AccessLevel.PRIVATE)
    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private OrderStatusEnum status = OrderStatusEnum.OPEN;

    @Column(name = "discount")
    private BigDecimal discount = BigDecimal.ZERO;

    @Setter(value = AccessLevel.PRIVATE)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id")
    private List<OrderItem> items;

    @CreationTimestamp
    @Setter(value = AccessLevel.PRIVATE)
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Setter(value = AccessLevel.PRIVATE)
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    public OrderItem addItem(Product product, Integer quantity, BigDecimal unitaryValue) {
        validateStatus();

        Optional<OrderItem> itemByProduct = getItemByProduct(product);
        if (itemByProduct.isPresent()) {
            throw new ProductAlreadyAddedToOrderException(
                    MessageFormat.format("Product [{0}] has already been added to order.", product.getId()));

        } else {
            if (CollectionUtils.isEmpty(this.items)) {
                this.items = new ArrayList<>();
            }

            OrderItem orderItem = new OrderItem();
            orderItem.setProduct(product);
            orderItem.setQuantity(quantity);
            orderItem.setUnitaryValue(unitaryValue);
            this.items.add(orderItem);
            return orderItem;
        }
    }

    public void editItem(Long orderItemId, Product product, Integer quantity, BigDecimal unitaryValue) {
        validateStatus();

        Optional<OrderItem> orderItem = getItemById(orderItemId);
        if (!orderItem.isPresent()) {
            throw new OrderItemNotFoundInOrderException(
                    MessageFormat.format("OrderItem [id: {0}] was not found on order [{1}].", orderItemId, this.id));
        }

        Optional<OrderItem> itemByProduct = getItemByProduct(product);
        if (itemByProduct.isPresent() && !itemByProduct.get().getId().equals(orderItemId)) {
            throw new ProductAlreadyAddedToOrderException(
                    MessageFormat.format("Product [{0}] has already been added to order.", product.getId()));
        }

        orderItem.get().setProduct(product);
        orderItem.get().setQuantity(quantity);
        orderItem.get().setUnitaryValue(unitaryValue);
    }

    public void removeItem(Long orderItemId) {
        validateStatus();

        Optional<OrderItem> itemByProduct = getItemById(orderItemId);
        if (!itemByProduct.isPresent()) {
            throw new ProductNotFoundInOrderException(
                    MessageFormat.format("OrderItem [id: {0}] was not found on order [id: {1}].", orderItemId,
                            this.id));
        }

        this.items.remove(itemByProduct.get());
    }

    public List<OrderItem> getItems() {
        if (CollectionUtils.isEmpty(this.items)) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(this.items);
    }

    public Optional<OrderItem> getItemByProduct(Product product) {
        if (CollectionUtils.isEmpty(this.items)) {
            return Optional.empty();
        }

        for (OrderItem item : this.items) {
            if (item.getProduct().equals(product)) {
                return Optional.of(item);
            }
        }

        return Optional.empty();
    }

    public Optional<OrderItem> getItemById(Long ordemItemId) {
        if (CollectionUtils.isEmpty(this.items)) {
            return Optional.empty();
        }

        for (OrderItem item : this.items) {
            if (item.getId().equals(ordemItemId)) {
                return Optional.of(item);
            }
        }

        return Optional.empty();
    }

    private void validateStatus() {
        if (OrderStatusEnum.CLOSED.equals(this.status)) {
            throw new ClosedOrderException("You cannot change a closed order.");
        }
    }

    public BigDecimal getAmount() {
        if (CollectionUtils.isEmpty(this.items)) {
            return BigDecimal.ZERO;
        }

        return this.items.stream()
                .map(OrderItem::getAmount)
                .reduce(BigDecimal.ZERO.subtract(this.discount), BigDecimal::add);
    }

    public void closeOrder() {
        validateStatus();
        this.status = OrderStatusEnum.CLOSED;
    }

    public void setDiscount(BigDecimal discount) {
        validateStatus();

        if (Objects.nonNull(discount)) {
            if (BigDecimal.ZERO.compareTo(discount) > 0) {
                throw new DiscountAmountException("It is not possible to assign a negative value to the discount.");
            }
            this.discount = discount;
        }
    }
}
