package br.com.amarques.ecommerce.store.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class ApplicationExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<Object> handleException(NotFoundException exception) {
        log.info(exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = ClosedOrderException.class)
    public ResponseEntity<Object> handleException(ClosedOrderException exception) {
        log.info(exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = EmailAlreadyRegisteredException.class)
    public ResponseEntity<Object> handleException(EmailAlreadyRegisteredException exception) {
        log.info(exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = ProductAlreadyAddedToOrderException.class)
    public ResponseEntity<Object> handleException(ProductAlreadyAddedToOrderException exception) {
        log.info(exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = ProductNotFoundInOrderException.class)
    public ResponseEntity<Object> handleException(ProductNotFoundInOrderException exception) {
        log.info(exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = OrderItemNotFoundInOrderException.class)
    public ResponseEntity<Object> handleException(OrderItemNotFoundInOrderException exception) {
        log.info(exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = DiscountAmountException.class)
    public ResponseEntity<Object> handleException(DiscountAmountException exception) {
        log.info(exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }
}
